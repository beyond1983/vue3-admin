import {createRouter,createWebHistory} from 'vue-router'
import layout from '../views/layout/index.vue'
import home from '../views/Home.vue'
import about from '../views/About.vue'
import login from '../views/Login.vue'

export default createRouter({
    history:createWebHistory(),
    routes:[
        {
            path:'/',
            component:layout,
            children:[
                {
                    path:'/index',
                    component:home
                }
                ,{
                    path:'/about',
                    component:about
                }
            ]
        }
        ,{
            path:'/login',
            component:login
        }
    ]
})