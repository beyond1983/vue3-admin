import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import createRouter from './router'
import store from './store'

const vue=createApp(App).use(store);
vue.use(ElementPlus);
vue.use(createRouter);
vue.mount('#app');
